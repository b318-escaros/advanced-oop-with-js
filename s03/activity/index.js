// Activity 1
// Abstract class RegularShape
class RegularShape {
  constructor() {
    if (this.constructor === RegularShape) {
      throw new Error('Object cannot be created from an abstract class RegularShape');
    }
    if (this.calculatePerimeter === undefined) {
      throw new Error('Class must implement calculatePerimeter() method');
    }
    if (this.calculateArea === undefined) {
      throw new Error('Class must implement calculateArea() method');
    }
  }

  getPerimeter() {
    return `The perimeter of the square is ${this.calculatePerimeter()}`;
  }

  getArea() {
    return `The area of the square is ${this.calculateArea()}`;
  }
}

// Class Square inheriting from RegularShape
class Square extends RegularShape {
  constructor(length) {
    super();
    this.length = length;
  }

  calculatePerimeter() {
    return 4 * this.length;
  }

  calculateArea() {
    return this.length ** 2;
  }
}

// Test case
const shape1 = new Square(16);
console.log(shape1.getPerimeter()); // The perimeter is 64
console.log(shape1.getArea()); // The area is 256



// Activity 2
class Food {
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
}

class Vegetable extends Food {
  constructor(name, breed, price) {
    super(name, price);
    this.breed = breed;
  }

  getName() {
    return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`;
  }
}

const vegetable1 = new Vegetable("Pechay", "Native", 25);
console.log(vegetable1.getName()); // Pechay is of Native variety and is priced at 25 pesos.


// Activity 3
// Abstract class Equipment
class Equipment {
  constructor(equipmentType, model) {
    if (this.constructor === Equipment) {
      throw new Error('Object cannot be created from an abstract class Equipment');
    }
    this.equipmentType = equipmentType;
    this.model = model;
  }

  printlnfo() {
    throw new Error('Method printlnfo() must be implemented in derived classes');
  }
}

// Class Bulldozer inheriting from Equipment
class Bulldozer extends Equipment {
  constructor(model, bladeType) {
    super('Bulldozer', model);
    this.bladeType = bladeType;
  }

  printlnfo() {
    return `Info: ${this.equipmentType}\nThe bulldozer ${this.model} has a ${this.bladeType} blade.`;
  }
}

// Class TowerCrane inheriting from Equipment
class TowerCrane extends Equipment {
  constructor(model, hookRadius, maxCapacity) {
    super('Tower Crane', model);
    this.hookRadius = hookRadius;
    this.maxCapacity = maxCapacity;
  }

  printlnfo() {
    return `Info: ${this.equipmentType}\nThe tower crane ${this.model} has a ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity.`;
  }
}

// Test cases
const bulldozer1 = new Bulldozer("Brute", "Shovel");
const towercrane1 = new TowerCrane("Pelican", 100, 1500);

console.log(bulldozer1.printlnfo());
// Info: Bulldozer
// The bulldozer Brute has a Shovel blade.

console.log(towercrane1.printlnfo());
// Info: Tower Crane
// The tower crane Pelican has a 100 cm hook radius and 1500 kg max capacity.
