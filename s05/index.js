// Abstract Person class
class Person {
  constructor(firstName, lastName) {
    if (this.constructor === Person) {
      throw new Error('Object cannot be created from an abstract class Person.');
    }
    this.firstName = firstName;
    this.lastName = lastName;
  }

  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  login() {
    return `${this.firstName} ${this.lastName} has logged in`;
  }

  logout() {
    return `${this.firstName} ${this.lastName} has logged out`;
  }
}

// Request class
class Request {
  constructor(requesterEmail, content, dateRequested) {
    this.requesterEmail = requesterEmail;
    this.content = content;
    this.dateRequested = new Date(dateRequested);
  }
}

// Employee class
class Employee extends Person {
  #email;
  #department;
  #isActive;
  #requests;

  constructor(firstName, lastName, email, department, isActive = true) {
    super(firstName, lastName);
    this.#email = email;
    this.#department = department;
    this.#isActive = isActive;
    this.#requests = [];
  }

  setFirstName(firstName) {
    this.firstName = firstName;
  }

  getFirstName() {
    return this.firstName;
  }

  setLastName(lastName) {
    this.lastName = lastName;
  }

  getLastName() {
    return this.lastName;
  }

  setEmail(email) {
    this.#email = email;
  }

  getEmail() {
    return this.#email;
  }

  setDepartment(department) {
    this.#department = department;
  }

  getDepartment() {
    return this.#department;
  }

  setIsActive(isActive) {
    this.#isActive = isActive;
  }

  getIsActive() {
    return this.#isActive;
  }

  getRequests() {
    return this.#requests;
  }

  addRequest(content, dateRequested) {
    const request = new Request(this.#email, content, dateRequested);
    this.#requests.push(request);
  }
}

// TeamLead class
class TeamLead extends Person {
  #email;
  #department;
  #isActive;
  #members;

  constructor(firstName, lastName, email, department, isActive = true) {
    super(firstName, lastName);
    this.#email = email;
    this.#department = department;
    this.#isActive = isActive;
    this.#members = [];
  }

  setFirstName(firstName) {
    this.firstName = firstName;
  }

  getFirstName() {
    return this.firstName;
  }

  setLastName(lastName) {
    this.lastName = lastName;
  }

  getLastName() {
    return this.lastName;
  }

  setEmail(email) {
    this.#email = email;
  }

  getEmail() {
    return this.#email;
  }

  setDepartment(department) {
    this.#department = department;
  }

  getDepartment() {
    return this.#department;
  }

  setIsActive(isActive) {
    this.#isActive = isActive;
  }

  getIsActive() {
    return this.#isActive;
  }

  getMembers() {
    return this.#members;
  }

  addMember(employee) {
    this.#members.push(employee);
  }

  checkRequests(employeeEmail) {
    const matchingRequests = [];

    for (const member of this.#members) {
      if (member.getEmail() === employeeEmail) {
        matchingRequests.push(...member.getRequests());
      }
    }

    if (matchingRequests.length > 0) {
      return matchingRequests;
    } else {
      return `No requests found for employee with email: ${employeeEmail}`;
    }
  }
}

// Admin class
class Admin extends Person {
  #email;
  #department;
  #isActive;
  #teamLeads;

  constructor(firstName, lastName, email, department, isActive = true) {
    super(firstName, lastName);
    this.#email = email;
    this.#department = department;
    this.#isActive = isActive;
    this.#teamLeads = [];
  }

  setFirstName(firstName) {
    this.firstName = firstName;
  }

  getFirstName() {
    return this.firstName;
  }

  setLastName(lastName) {
    this.lastName = lastName;
  }

  getLastName() {
    return this.lastName;
  }

  setEmail(email) {
    this.#email = email;
  }

  getEmail() {
    return this.#email;
  }

  setDepartment(department) {
    this.#department = department;
  }

  getDepartment() {
    return this.#department;
  }

  setIsActive(isActive) {
    this.#isActive = isActive;
  }

  getIsActive() {
    return this.#isActive;
  }

  getTeamLeads() {
    return this.#teamLeads;
  }

  addTeamLead(teamLead) {
    this.#teamLeads.push(teamLead);
  }

  deactivateTeam(teamLeadEmail){
    for (const teamLead of this.#teamLeads) {
      if (teamLead.getEmail() === teamLeadEmail) {
        teamLead.setIsActive(false);
        for (const member of teamLead.getMembers()) {
          member.setIsActive(false);
        }
        break;
      }
    }
  }
}

// Instantiate an Employee
const employee = new Employee("John", "Doe", "john.doe@example.com", "Engineering");
employee.setIsActive(true);
employee.addRequest("Vacation leave", "2023-07-07");

console.log(employee.getFullName()); // Output: John Doe
console.log(employee.getEmail()); // Output: john.doe@example.com
console.log(employee.getDepartment()); // Output: Engineering
console.log(employee.getIsActive()); // Output: true
console.log(employee.getRequests()); // Output: [Request] contains content, requester's email, and dateRequested

// Instantiate a TeamLead
const teamLead = new TeamLead("Jane", "Smith", "jane.smith@example.com", "Engineering");
teamLead.setIsActive(true);
teamLead.addMember(employee);

console.log(teamLead.getFullName()); // Output: Jane Smith
console.log(teamLead.getEmail()); // Output: jane.smith@example.com
console.log(teamLead.getDepartment()); // Output: Engineering
console.log(teamLead.getIsActive()); // Output: true
console.log(teamLead.getMembers()); // Output: [Employee]

// Instantiate an Admin
const admin = new Admin("Admin", "User", "admin@example.com", "Administration");
admin.setIsActive(true);
admin.addTeamLead(teamLead);

console.log(admin.getFullName()); // Output: Admin User
console.log(admin.getEmail()); // Output: admin@example.com
console.log(admin.getDepartment()); // Output: Administration
console.log(admin.getIsActive()); // Output: true
console.log(admin.getTeamLeads()); // Output: [TeamLead]

// Invoke checkRequests() 
console.log(teamLead.checkRequests("jane.smith@example.com")); // Output: [Request] contains content, requester's email, and dateRequested

// Invoke deactivateTeam()
admin.deactivateTeam("jane.smith@example.com");
console.log(teamLead.getIsActive()); // Output: false
console.log(employee.getIsActive()); // Output: false
