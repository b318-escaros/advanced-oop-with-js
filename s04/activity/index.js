// Activity 1
// Abstract class RegularShape
class RegularShape {
  constructor() {
    if (this.constructor === RegularShape) {
      throw new Error('RegularShape cannot be instantiated directly.');
    }
  }

  getPerimeter() {
    throw new Error('getPerimeter() method must be implemented in derived classes.');
  }

  getArea() {
    throw new Error('getArea() method must be implemented in derived classes.');
  }
}

// Class Square inheriting from RegularShape
class Square extends RegularShape {
  #noSides;
  #length;

  constructor() {
    super();
    this.#noSides = 0;
    this.#length = 0;
  }

  setNoSides(noSides) {
    this.#noSides = noSides;
  }

  setLength(length) {
    this.#length = length;
  }

  getLength() {
    return this.#length;
  }

  getPerimeter() {
    return `The perimeter of the square is ${this.#noSides * this.#length}`;
  }

  getArea() {
    return `The area of the square is ${this.#length ** 2}`;
  }
}

// Class Triangle inheriting from RegularShape
class Triangle extends RegularShape {
  #noSides;
  #length;

  constructor() {
    super();
    this.#noSides = 0;
    this.#length = 0;
  }

  setNoSides(noSides) {
    this.#noSides = noSides;
  }

  setLength(length) {
    this.#length = length;
  }

  getLength() {
    return this.#length;
  }

  getPerimeter() {
    return `The perimeter of the triangle is ${this.#noSides * this.#length}`;
  }

  getArea() {
    return `The area of the triangle is ${(Math.sqrt(3) / 4) * (this.#length ** 2)}`;
  }
}

const square1 = new Square();
square1.setNoSides(4);
square1.setLength(65);
console.log(square1.getLength()); // 65
console.log(square1.getPerimeter()); // The perimeter of the square is 260

const triangle1 = new Triangle();
triangle1.setNoSides(3);
triangle1.setLength(15);
console.log(triangle1.getLength()); // 15
console.log(triangle1.getArea()); // The area of the triangle is 97.428






// Activity 2
// Abstract class User
class User {
  constructor() {}

  login() {
    throw new Error('login() method must be implemented in derived classes.');
  }

  register() {
    throw new Error('register() method must be implemented in derived classes.');
  }

  logout() {
    throw new Error('logout() method must be implemented in derived classes.');
  }
}

// Class RegularUser inheriting from User
class RegularUser extends User {
  #name;
  #email;
  #password;

  constructor() {
    super();
    this.#name = '';
    this.#email = '';
    this.#password = '';
  }

  setName(name) {
    this.#name = name;
  }

  getName() {
    return this.#name;
  }

  setEmail(email) {
    this.#email = email;
  }

  getEmail() {
    return this.#email;
  }

  setPassword(password) {
    this.#password = password;
  }

  getPassword() {
    return this.#password;
  }

  login() {
    return `${this.#name} has logged in`;
  }

  register() {
    return `${this.#name} has registered`;
  }

  logout() {
    return `${this.#name} has logged out`;
  }

  browseJobs() {
    return 'There are 10 jobs found';
  }
}

// Class Admin inheriting from User
class Admin extends User {
  #name;
  #email;
  #password;
  #hasAdminExpired;

  constructor() {
    super();
    this.#name = '';
    this.#email = '';
    this.#password = '';
    this.#hasAdminExpired = false;
  }

  setName(name) {
    this.#name = name;
  }

  getName() {
    return this.#name;
  }

  setEmail(email) {
    this.#email = email;
  }

  getEmail() {
    return this.#email;
  }

  setPassword(password) {
    this.#password = password;
  }

  getPassword() {
    return this.#password;
  }

  setHasAdminExpired(hasExpired) {
    this.#hasAdminExpired = hasExpired;
  }

  getHasAdminExpired() {
    return this.#hasAdminExpired;
  }

  login() {
    return `Admin ${this.#name} has logged in`;
  }

  register() {
    return `Admin ${this.#name} has registered`;
  }

  logout() {
    return `Admin ${this.#name} has logged out`;
  }

  postJob() {
    return 'Job posting added to site';
  }
}

const regUser1 = new RegularUser();
regUser1.setName('Dan');
regUser1.setEmail('dan@mail.com');
regUser1.setPassword('Dan12345');
console.log(regUser1.register()); // Dan has registered.
console.log(regUser1.login()); // Dan has logged in.
console.log(regUser1.browseJobs()); // There are 10 jobs found.
console.log(regUser1.logout()); // Dan has logged out.

const admin = new Admin();
admin.setName('Joe');
admin.setEmail('admin_joe@mail.com');
admin.setPassword('joe12345');
admin.setHasAdminExpired(false);
console.log(admin.register()); // Admin Joe has registered.
console.log(admin.login()); // Admin Joe has logged in.
console.log(admin.getHasAdminExpired()); // false
console.log(admin.postJob()); // Job posting added to site.
console.log(admin.logout()); // Admin Joe has logged out.
