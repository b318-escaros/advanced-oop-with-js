class Person{
	constructor(/*firstName, lastName*/){
		if(this.constructor === Person){
			throw new Error(
				"You are not allowed to create a object using the Person class"
			);
			// this.firstName = firstName;
			// this.lastName = lastName;
		}
		if(this.getFullName === undefined){
			throw new Error(
				"Class must implement getFullName() method."
			);
		}
	}
}

class Employee extends Person {
	// Encapsulation aided by private fields (#), ensures data protection.
	// Setters and getters provide controlled access to encapsulation data.
	/*Private fields*/
	#firstName
	#lastName
	#employeeID

	constructor(firstName, lastName, employeeID){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#employeeID = employeeID;
	}
	getFirstName(){
		return `First Name: ${this.#firstName}`;
	}

	getLastName(){
		return `Last Name: ${this.#lastName}`;
	}
	getFullName(){
		return `${this.#firstName} ${this.#lastName} has employee ID of ${this.#employeeID}`;
	}
	setFirstName(firstName){
		this.#firstName = firstName;
	}
	setLastName(lastName){
		this.#lastName = lastName;
	}
	setEmployeeId(employeeID){
		this.#employeeID = employeeID;
	}


}

const employeeA = new Employee('John', 'Smith', 'EM-001')
// Direct access property firstName will return undefined bacause the property is in the private state.
// console.log(employeeA.firstName);

// We could directly change the value of the property firstName becase the property is private
console.log(employeeA.getFirstName());
employeeA.setFirstName("David");
console.log(employeeA.getFirstName());
console.log(employeeA.getFullName());





const employeeB = new Employee('Mann', 'Kiksa', 'EM-003');
employeeB.setFirstName("Jill")
employeeB.setLastName("Bill")
employeeB.setEmployeeId("EM-002")
console.log(employeeB.getFullName());


