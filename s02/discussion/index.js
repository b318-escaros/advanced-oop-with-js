// Polymorphism
// Involves the concept of overriding, Where a method in subclass (derived class) overides the implementation of a method with the same name in its superclass (base classs).

class Person {
	constructor(firstName, lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}

	getFullName(){
		return `The person's name is ${this.firstName} ${this.lastName}`;
	}
}

class Employee extends Person{
	constructor(firstName, lastName, employeeId){
		super(firstName, lastName)
		this.employeeId = employeeId;
	}
	// Overriding
	getFullName(){
		// return `The person's name is ${this.firstName} ${this.lastName} with employee ID ${this.employeeId}`;
		return super.getFullName() + ` with employee ID ${this.employeeId}`
	}

}

const employeeA = new Employee ('John', 'Smith', 'EM-004');
console.log(employeeA.getFullName());

class TeamLead extends Employee {
	getFullName(){
		return super.getFullName() + ` and he/she is a team lead`
	}

}

const teamLead = new TeamLead('Jane', 'Smith', 'TL-001')
console.log(teamLead.getFullName())



 