// ACTIVITY 1

// 1.) class Equipment
class Equipment {
  constructor(equipmentType) {
    this.equipmentType = equipmentType;
  }

  printlnfo() {
    return `Info: ${this.equipmentType}`;
  }
}

//2.) Class Bulldozer inheriting from Equipment
class Bulldozer extends Equipment {
  constructor(equipmentType, model, bladeType) {
    super(equipmentType);
    this.model = model;
    this.bladeType = bladeType;
  }

  printlnfo() {
    return `${super.printlnfo()}\nThe bulldozer ${this.model} has a ${this.bladeType} blade.`;
  }
}

//3.) Class TowerCrane inheriting from Equipment
class TowerCrane extends Equipment {
  constructor(equipmentType, model, hookRadius, maxCapacity) {
    super(equipmentType);
    this.model = model;
    this.hookRadius = hookRadius;
    this.maxCapacity = maxCapacity;
  }

  printlnfo() {
    return `${super.printlnfo()}\nThe tower crane ${this.model} has a ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity.`;
  }
}

//4.) Class BackLoader inheriting from Equipment
class BackLoader extends Equipment {
  constructor(equipmentType, model, type, tippingLoad) {
    super(equipmentType);
    this.model = model;
    this.type = type;
    this.tippingLoad = tippingLoad;
  }

  printlnfo() {
    return `${super.printlnfo()}\nThe loader ${this.model} is a ${this.type} loader and has a tipping load of ${this.tippingLoad} lbs.`;
  }
}

// Test cases
let bulldozerl = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozerl.printlnfo());

let towercranel = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(towercranel.printlnfo());

let backLoaderl = new BackLoader("back loader", "Turtle", "hydraulic", 1500);
console.log(backLoaderl.printlnfo());


// ACTIVITY 2

//5.) Class RegularShape
class RegularShape {
  constructor(noSides, length) {
    this.noSides = noSides;
    this.length = length;
  }

  getPerimeter() {
    return `The perimeter is ${this.noSides * this.length}`;
  }

  getArea() {
    return `The area is ${this.calculateArea()}`;
  }

  calculateArea() {
    throw new Error('calculateArea() method must be implemented in derived classes.');
  }
}

//6.) Class Triangle inheriting from RegularShape
class Triangle extends RegularShape {
  constructor(noSides, length) {
    super(noSides, length);
  }

  calculateArea() {
    const height = Math.sqrt(3) * this.length / 2;
    return (0.5 * this.length * height).toString();
  }
}

//7.) Class Square inheriting from RegularShape
class Square extends RegularShape {
  constructor(noSides, length) {
    super(noSides, length);
  }

  calculateArea() {
    return (this.length ** 2).toString();
  }
}

// Test cases
let triangle1 = new Triangle(3, 10);
console.log(triangle1.getPerimeter()); // The perimeter is 30
console.log(triangle1.getArea()); // The area is 43.30127018922193

let square1 = new Square(4, 12);
console.log(square1.getPerimeter()); // The perimeter is 48
console.log(square1.getArea()); // The area is 144
